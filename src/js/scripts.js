window.addEventListener('DOMContentLoaded', start);

function start() {
	showRandomImage();
	fetchData();
}

// str.match(/\b(\w*work\w*)\b/gi);

document.querySelector('.submit-button').addEventListener('click', (e) => {
	e.preventDefault();
	searchWord();
});

document.querySelector('.search-input').addEventListener('keyup', (e) => {
	if (e.keyCode == 13) {
		// 13 is enter
		searchWord();
	}
});

function searchWord() {
	let wordToSearch = document.querySelector('.search-input').value;
	window.find(wordToSearch);
}

function showRandomImage() {
	let randomSelector = Math.floor(Math.random() * 4 + 0);
	let sliderImages = document.querySelectorAll('.carousel-element');
	sliderImages[randomSelector].classList.remove('fade');

	setInterval(moveSlider, 3000);
	let totalImages = sliderImages.length - 1;
	let imgNum = randomSelector;

	function moveSlider() {
		if (imgNum < totalImages) {
			sliderImages[imgNum + 1].classList.add('transparency');
			sliderImages[imgNum + 1].classList.remove('fade');
			sliderImages[imgNum].classList.add('fade');
			imgNum = imgNum + 1;
		} else if (imgNum == totalImages) {
			sliderImages[0].classList.remove('fade');
			sliderImages[imgNum].classList.add('fade');
			sliderImages[0].classList.add('transparency');
			imgNum = 0;
		}
	}
}

// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript

function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (Math.random() * 16) | 0,
			v = c == 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}

// Fetch and populate

let template = document.querySelector('template').content;
let parent = document.querySelector('.articles-container');
const baseLink = 'js/articles.json';

function fetchData() {
	fetch(baseLink).then((e) => e.json()).then((data) => showData(data));
}

function showData(data) {
	let j = 2;
	for (let i = 0; i < 5; i++) {
		if (j < 6) {
			let buttonParent = document.querySelector('.buttons-container');
			let button = document.createElement('button');
			button.textContent = j;
			buttonParent.appendChild(button);
			j++;
		}
		document.querySelector('.last-page').textContent = j - 1;
		data['data'].forEach((article) => {
			let clone = template.cloneNode(true);

			clone.querySelector('[data-uuid=unique-id]').dataset.uuid = uuidv4();
			clone.querySelector('.image-post').src = 'img/image-post-' + article.img + '.png';
			clone.querySelector('.header-post').textContent = article.headerPost;
			clone.querySelector('.date-post').textContent = article.datePost;
			if (article.descriptionPost.length < 180) {
				clone.querySelector('.description-post').textContent = article.datePost;
			} else if (article.descriptionPost.length > 180) {
				let stringContainer = '( page ' + i + ' )' + article.descriptionPost;
				stringContainer = stringContainer.slice(0, 180);
				stringContainer += '...';
				clone.querySelector('.description-post').textContent = stringContainer;
			}

			clone.querySelector('.description-post-hidden').textContent = article.descriptionPost;
			clone.querySelector('.button-post').addEventListener('click', toggleShow);
			parent.appendChild(clone);
		});
	}
	document.querySelectorAll('.buttons-container button').forEach((el) => (el.style.marginLeft = '5px'));
	for (let i = 0; i < 3; i++) {
		document.querySelectorAll('.articles-container article')[i].classList.remove('fade');
	}
	document.querySelectorAll('.buttons-container button').forEach((el) => {
		el.addEventListener('click', loadArticlePage);
	});
}

function toggleShow() {
	if (event.target.classList.contains('untoggled') == true) {
		event.target.parentElement.querySelector('.description-post-hidden').classList.remove('fade');
		event.target.parentElement.querySelector('.description-post').classList.add('fade');
		event.target.textContent = 'Minimise';
		event.target.classList.remove('untoggled');
	} else if (event.target.classList.contains('untoggled') == false) {
		event.target.parentElement.querySelector('.description-post-hidden').classList.add('fade');
		event.target.parentElement.querySelector('.description-post').classList.remove('fade');
		event.target.textContent = 'Minimise';
		event.target.classList.add('untoggled');
	}
}

function loadArticlePage() {
	let pageSelNum = event.target.textContent;
	for (let i = pageSelNum * 3 - 3; i < pageSelNum * 3; i++) {
		document.querySelectorAll('.articles-container article')[i].classList.remove('fade');
	}
	let pageCurNum = document.querySelector('.active-page').textContent;
	let buttonBefore = document.querySelector('.active-page');
	buttonBefore.classList.remove('active-page');
	event.target.classList.add('active-page');
	for (let i = pageCurNum * 3 - 3; i < pageCurNum * 3; i++) {
		document.querySelectorAll('.articles-container article')[i].classList.add('fade');
	}
}
